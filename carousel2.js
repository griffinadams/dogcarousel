//DO NOT USE THIS

$.Carousel = function (el) {
  this.$el = $(el);
  this.activeIdx = 0;
  this.activateFirst();
  this.$prev = this.$el.find('.slide-right');
  this.$next = this.$el.find('.slide-left');
  this.bindHandlers();
};

$.Carousel.prototype.activateFirst = function() {
  var $firstLi = this.$el.find('.items').children().first();
  $firstLi.addClass('active');
};

$.Carousel.prototype.bindHandlers = function() {
  this.$prev.on('click', function(event){
    this.slideRight()}.bind(this));

  this.$next.on('click', function(event){
    this.slideLeft()}.bind(this));
};

$.Carousel.prototype.slideLeft = function () {
  var $activeLi = this.$el.find('.active');
  $activeLi.toggleClass('active');
  var newIndex = this.slide(1);

  var nextLi = this.$el.find('.items').children().eq(newIndex);
  nextLi.toggleClass('active');

};

$.Carousel.prototype.slideRight= function () {
  var $activeLi = this.$el.find('.active');
  $activeLi.toggleClass('active');
  var newIndex = this.slide(-1);

  var nextLi = this.$el.find('.items').children().eq(newIndex);
  nextLi.toggleClass('active');
};

$.Carousel.prototype.slide = function (dir) {
  var newAbsIndex = this.activeIdx + dir;
  return (newAbsIndex % this.$el.find('.items').children().length);
}

$.fn.carousel = function () {
  return this.each(function () {
    new $.Carousel(this);
  });
};
