$.Carousel = function (el) {
  this.$el = $(el);
  this.activeIdx = 0;
  this.length = this.$el.find('.items').children().length;
  this.$prev = this.$el.find('.slide-right');
  this.$next = this.$el.find('.slide-left');
  this.bindHandlers();
};

$.Carousel.prototype.bindHandlers = function() {
  this.$prev.on('click', function(event){
    this.slide(-1)}.bind(this));

  this.$next.on('click', function(event){
    this.slide(1)}.bind(this));
};

$.Carousel.prototype.slide = function (dir) {
  var newAbsIndex = this.activeIdx + dir;
  this.activeIdx = ((newAbsIndex + this.length) % this.length);
  var margin = this.activeIdx * (-260);
  this.$el.find('.items').css({'margin-left': margin});
}

$.fn.carousel = function () {
  return this.each(function () {
    new $.Carousel(this);
  });
};
